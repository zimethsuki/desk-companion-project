﻿
using UnityEngine;
using Handler = System.Action<System.Object, System.Object>;

public static class ReNotificationExtensions
{
    public static void PostNotification(this object obj, string notificationName)
    {

        ReNotificationCenter.instance.PostNotification(notificationName, obj);
    }

    public static void PostNotification(this object obj, string notificationName, object e)
    {
        ReNotificationCenter.instance.PostNotification(notificationName, obj, e);
    }

    public static void AddObserver(this object obj, Handler handler, string notificationName)
    {
        ReNotificationCenter.instance.AddObserver(handler, notificationName);
    }

    public static void AddObserver(this object obj, Handler handler, string notificationName, object sender)
    {
        ReNotificationCenter.instance.AddObserver(handler, notificationName, sender);
    }

    public static void RemoveObserver(this object obj, Handler handler, string notificationName)
    {
        ReNotificationCenter.instance.RemoveObserver(handler, notificationName);
    }

    public static void RemoveObserver(this object obj, Handler handler, string notificationName, System.Object sender)
    {
        ReNotificationCenter.instance.RemoveObserver(handler, notificationName, sender);
    }
}