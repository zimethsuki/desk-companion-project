using System;
using System.Runtime.InteropServices;

public class Win32API
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getforegroundwindow
    /// </summary>
    [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
    public static extern IntPtr GetForegroundWindow();

    private const int
         SW_HIDE = 0, // hide window and taskbar
         SW_SHOW = 5; // Display the current size and position

    /// <summary>
    /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
    /// </summary>
    [DllImport("user32.dll")]
    public static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);
    public static void Hide(IntPtr hwnd) => ShowWindow(hwnd, SW_HIDE);
    public static void Show(IntPtr hwnd) => ShowWindow(hwnd, SW_SHOW);

    [DllImport("Dwmapi.dll", EntryPoint = "DwmExtendFrameIntoClientArea")]
    public static extern int DwmExtendFrameIntoClientArea(IntPtr hwnd, ref Margins margins);

    [DllImport("user32.dll", EntryPoint = "SetFocus", CharSet = CharSet.Auto, ExactSpelling = true)]
    public static extern IntPtr SetFocus(IntPtr hwnd);

    [DllImport("user32.dll", EntryPoint = "GetActiveWindow")]
    public static extern IntPtr GetActiveWindow();

    [DllImport("user32.dll", EntryPoint = "FindWindow", CharSet = CharSet.Auto)]
    public static extern int FindWindow(string strClassName, string strWindowName);

    [DllImport("user32.dll", EntryPoint = "GetWindowRect")]
    public static extern bool GetWindowRect(IntPtr hwnd, out Rectangle rect);

    [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
    public static extern bool SetWindowPos(IntPtr hwnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

    [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
    public static extern long GetWindowLong(IntPtr hwnd, int nIndex);

    [DllImport("user32.dll", EntryPoint = "SetWindowLongA")]
    public static extern int SetWindowLong(IntPtr hwnd, int nIndex, long dwNewLong);

    [DllImport("user32.dll", EntryPoint = "SetLayeredWindowAttributes")]
    public static extern int SetLayeredWindowAttributes(IntPtr hwnd, int crKey, byte bAlpha, int dwFlags);

    [DllImport("user32.dll", EntryPoint = "MoveWindow")]
    public static extern bool MoveWindow(IntPtr hwnd, int x, int Y, int nWidth, int nHeight, bool bRepaint);

    [DllImport("user32.dll", EntryPoint = "GetCursorPos")]
    public static extern bool GetCursorPos(out int X, out int Y);

}

[StructLayout(LayoutKind.Sequential)]
public struct Rectangle
{
    public int Left;
    public int Top;
    public int Right;
    public int Bottom;
}

public struct Margins
{
    public int cxLeftWidth;
    public int cxRightWidth;
    public int cyTopHeight;
    public int cyBottomHeight;
}