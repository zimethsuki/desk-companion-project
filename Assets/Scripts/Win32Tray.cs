using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using UnityEngine;

public class Win32Tray : IDisposable
{
    private const int WIDTH = 40;
    private const int HEIGHT = 40;
    private IntPtr hwnd;

    private bool AIisOn = true;

    private NotifyIcon notifyIcon; // tray icon
    private ContextMenuStrip contextMenu; // Context menu

    private ToolStripMenuItem menuItem_ChaseMouse; // Chase Mouse
    private ToolStripMenuItem menuItem_TurnAIOnOff; // Exit the program
    private ToolStripMenuItem menuItem_Quit; // Exit the program

    public void InitTray()
    {
        // int displayLength = Display.displays.Length;
        this.hwnd = Win32API.GetForegroundWindow();
        this.notifyIcon = new NotifyIcon();
        this.contextMenu = new ContextMenuStrip();
        this.menuItem_ChaseMouse = new ToolStripMenuItem();
        this.menuItem_TurnAIOnOff = new ToolStripMenuItem();
        this.menuItem_Quit = new ToolStripMenuItem();
        this.contextMenu.SuspendLayout();

        #region Notification icon

        this.notifyIcon.ContextMenuStrip = contextMenu;
        this.notifyIcon.Text = "Desk Companion - Pramanix";

        var iconPath = Path.Combine(UnityEngine.Application.streamingAssetsPath, "Icon.png");

        if (File.Exists(iconPath))
            this.notifyIcon.Icon = this.CustomTrayIcon(iconPath, WIDTH, HEIGHT);
        else
        {
            var bytes = Resources.Load<Texture2D>("Icon/Icon").EncodeToPNG();
            this.notifyIcon.Icon = CustomTrayIcon(ByteArrayToImage(bytes), WIDTH, HEIGHT);
        }
        this.notifyIcon.MouseClick += this.NotifyIcon_MouseClick;
        this.notifyIcon.Visible = true;
        //  this.notifyIcon.ShowBalloonTip(2000, "Bubble Title", "Bubble Tip", ToolTipIcon.Info);

        #endregion Notification icon

        //
        // contextMenu
        //
        List<ToolStripMenuItem> menuItems = new List<ToolStripMenuItem>
            {
                this.menuItem_ChaseMouse,
                this.menuItem_TurnAIOnOff,
                this.menuItem_Quit
            };

        this.contextMenu.Items.AddRange(menuItems.ToArray());
        this.contextMenu.Size = new Size(181, (menuItems.Count * 22) + 20);

        #region Chase Mouse Command

        this.menuItem_ChaseMouse.Size = new Size(180, 22);
        this.menuItem_ChaseMouse.Text = "Chase Mouse";
        this.menuItem_ChaseMouse.Click += (sender, e) =>
        {
            this.PostNotification(ReMessageCenter.TRAY_WINDOW_EVENT, TrayMessages.ChaseMouse);
        };

        #endregion Chase Mouse Command

        #region Turn AI OnOff Command

        this.menuItem_TurnAIOnOff.Size = new Size(180, 22);
        this.menuItem_TurnAIOnOff.Text = AIisOn ? "Turn AI Off" : "Turn AI On";
        this.menuItem_TurnAIOnOff.Click += (sender, e) =>
        {
            AIisOn = !AIisOn;
            this.PostNotification(ReMessageCenter.TRAY_WINDOW_EVENT, TrayMessages.AIOnOff);
        };

        #endregion Turn AI OnOff Command

        #region Quit App

        this.menuItem_Quit.Size = new Size(180, 22);
        this.menuItem_Quit.Text = "Quit";
        this.menuItem_Quit.Click += (sender, e) =>
        {
            this.PostNotification(ReMessageCenter.TRAY_WINDOW_EVENT, TrayMessages.QuitApp);
        };

        #endregion Quit App

        this.contextMenu.ResumeLayout(false);
    }

    private Icon CustomTrayIcon(string iconPath, int width, int height)
    {
        Bitmap bt = new Bitmap(iconPath);
        Bitmap fitSizeBt = new Bitmap(bt, width, height);
        return Icon.FromHandle(fitSizeBt.GetHicon());
    }

    private Icon CustomTrayIcon(Image img, int width, int height)
    {
        Bitmap bt = new Bitmap(img);
        Bitmap fitSizeBt = new Bitmap(bt, width, height);
        return Icon.FromHandle(fitSizeBt.GetHicon());
    }

    private Image ByteArrayToImage(byte[] byteArrayIn)
    {
        if (byteArrayIn == null)
            return null;
        using (MemoryStream ms = new MemoryStream(byteArrayIn))
        {
            Image returnImage = Image.FromStream(ms);
            ms.Flush();
            return returnImage;
        }
    }

    private void NotifyIcon_MouseClick(object sender, MouseEventArgs e)
    {
        if (e.Button == MouseButtons.Left)
        {
            Win32API.Show(this.hwnd);
        }
    }

    private void DestroyClick() => notifyIcon.MouseDoubleClick -= NotifyIcon_MouseClick;

    public void ShowTray() => notifyIcon.Visible = true;//Whether the tray button is visible

    public void HideTray() => notifyIcon.Visible = false;//Whether the tray button is visible

    public void Dispose()
    {
        DestroyClick();
        notifyIcon?.Dispose();
        contextMenu?.Dispose();
        menuItem_ChaseMouse?.Dispose();
        menuItem_TurnAIOnOff?.Dispose();
        menuItem_Quit?.Dispose();
        this.hwnd = IntPtr.Zero;
    }
}