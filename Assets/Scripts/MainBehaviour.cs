using MEC;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainBehaviour : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
{
    #region Dll Bridge

    private IntPtr ownHwnd;
    private Rectangle ownRect;

#if UNITY_STANDALONE_WIN || UNITY_EDITOR

    private const int HWND_TOPMOST = -1;
    private const int GWL_STYLE = -16;
    private const int GWL_EXSTYLE = -20;
    private const uint WS_POPUP = 0x80000000;
    private const uint WS_VISIBLE = 0x10000000;
    private const int WS_EX_TOOLWINDOW = 0x0080;
    // private const uint WS_EX_LAYERED = 0x80000000;
    // private const uint WS_EX_TRANSPARENT = 0x80000000;

#endif

    #endregion Dll Bridge

    #region General Variables

    private int step = 1;
    private int mousePosX;
    private int mousePosY;

    private int winHeight;
    private int winWidth;
    private int winHeightOffset;
    private int winWidthOffset;
    private int winCurrentPosX;
    private int winCurrentPosY;

    private Camera mainCam;

    #endregion General Variables

    #region Unity Methods

    private void Start()
    {
        mainCam = Camera.main;
        ContextWindowOpen(false);
        ChangeState(AIState.Thinking);
        ownHwnd = Win32API.GetActiveWindow();
        winHeight = 250;
        winWidth = 250;
        winHeightOffset = 150;
        winWidthOffset = 125;
        print(ownHwnd);

#if !UNITY_EDITOR && UNITY_STANDALONE_WIN

        Win32API.SetWindowLong(ownHwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);
        Win32API.SetWindowLong(ownHwnd, GWL_EXSTYLE, WS_EX_TOOLWINDOW);
        Margins margins = new Margins { cxLeftWidth = -1 };
        Win32API.DwmExtendFrameIntoClientArea(ownHwnd, ref margins);
        Win32API.SetWindowPos(ownHwnd, HWND_TOPMOST, 0, 0, winWidth, winHeight, 32 | 64);

#endif

    }

    private void OnEnable()
    {
        this.AddObserver(OnAnimationEventDone, ReMessageCenter.ANIMATION_EVENT_DONE);
        this.AddObserver(OnTrayCommandReceived, ReMessageCenter.TRAY_WINDOW_EVENT);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnAnimationEventDone, ReMessageCenter.ANIMATION_EVENT_DONE);
        this.RemoveObserver(OnTrayCommandReceived, ReMessageCenter.TRAY_WINDOW_EVENT);
    }

#if !UNITY_EDITOR && UNITY_STANDALONE_WIN

    Win32Tray tray;
 //   Win32ContexMenu floatingContext;
    private void Awake()
    {
        tray = new Win32Tray();
       // floatingContext = new Win32ContexMenu();
       // floatingContext.InitTray();
        tray.InitTray();
    }

    private void OnApplicationQuit()
    {
        tray?.Dispose();
        tray = null;
    }

#endif

    private bool clickThrough;
    private bool prevClickThrough;

    private void Update()
    {
        if (!lagiDiangkat)
        {
            clickThrough = !EventSystem.current.IsPointerOverGameObject() && !Physics2D.Raycast(mainCam.ScreenPointToRay(Input.mousePosition).origin, mainCam.ScreenPointToRay(Input.mousePosition).direction, 10f);
            // clickThrough = !EventSystem.current.IsPointerOverGameObject();

            if (clickThrough != prevClickThrough)
            {

                if (clickThrough)
                {
#if !UNITY_EDITOR

                /*
                if(contextOpen)
                {
                    contextOpen = false;
                    floatingContext.Hide();
                }
                */

                    Win32API.SetWindowLong(ownHwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);
                    Win32API.SetWindowLong(ownHwnd, GWL_EXSTYLE, (uint)524288 | (uint)32 | WS_EX_TOOLWINDOW);//WS_EX_LAYERED=524288=&h80000, WS_EX_TRANSPARENT=32=0x00000020L
                    Win32API.SetLayeredWindowAttributes(ownHwnd, 0, 255, 2);// Transparency=51=20%, LWA_ALPHA=2
                    Win32API.SetWindowPos(ownHwnd, HWND_TOPMOST, winCurrentPosX, winCurrentPosY, winWidth, winHeight, 32 | 64); //SWP_FRAMECHANGED = 0x0020 (32); //SWP_SHOWWINDOW = 0x0040 (64)
#endif
                }
                else
                {
#if !UNITY_EDITOR
                    Win32API.SetWindowLong (ownHwnd, GWL_EXSTYLE, ~(((uint)524288) | ((uint)32)) | WS_EX_TOOLWINDOW);//WS_EX_LAYERED=524288=&h80000, WS_EX_TRANSPARENT=32=0x00000020L
                    Win32API.SetWindowPos(ownHwnd, HWND_TOPMOST, winCurrentPosX, winCurrentPosY, winWidth, winHeight, 32 | 64); //SWP_FRAMECHANGED = 0x0020 (32); //SWP_SHOWWINDOW = 0x0040 (64)
#endif
                }

                prevClickThrough = clickThrough;

            }
        }

        /*
        GetWindowRect(ownHwnd, out ownRect);
        windowCoordinateText.text = ownRect.Left + "," + ownRect.Right + "," + ownRect.Bottom + "," + ownRect.Top;
        mouseCoordinateText.text = mousePosX + "," + mousePosY;
        */

    }

    #endregion Unity Methods

    #region Events

    #region Event Received

    private void OnAnimationEventDone(object sender, object data)
    {
        switch ((AnimationState)data)
        {
            case AnimationState.Jatoh:
            case AnimationState.Dijitak:
                ChangeState(AIState.Relaxing);
                bisaDijitak = true;
                bisaDiangkat = true;
                break;
        }
    }

    private void OnTrayCommandReceived(object sender, object data)
    {

        contextOpen = false;

        switch ((TrayMessages)data)
        {
            case TrayMessages.ChaseMouse:
                MoveToMouse();
                break;

            case TrayMessages.AIOnOff:

                break;

            case TrayMessages.QuitApp:
                CloseApp();
                break;
        }

    }

    #endregion Event Received

    #region Click Reponse

    private bool bisaDijitak = true;
    private bool bisaDiangkat = true;
    private bool lagiDiangkat = false;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            /*
#if !UNITY_EDITOR && UNITY_STANDALONE_WIN

            contextOpen = true;
            Win32API.GetCursorPos(out mousePosX, out mousePosY);
            floatingContext.Show(mousePosX, mousePosY);
#endif
            */

            ContextWindowOpen(true);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (!contextOpen && bisaDiangkat)
                Timing.RunCoroutine(HeldCounter(), "HeldDownCounter");
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            /*
#if !UNITY_EDITOR && UNITY_STANDALONE_WIN

            if(contextOpen)
            {
                contextOpen = false;
                floatingContext.Hide();
            }
#endif
            */

            Timing.KillCoroutines("HeldDownCounter");

            if (!contextOpen && bisaDijitak && !lagiDiangkat)
            {
                bisaDijitak = false;
                bisaDiangkat = false;
                ChangeState(AIState.Hit);
            }

            if (lagiDiangkat)
            {
                lagiDiangkat = false;
            }
        }
    }

    private IEnumerator<float> HeldCounter()
    {
        yield return Timing.WaitForSeconds(.5f);
        ChangeState(AIState.Picked);

    }

    #region Context Window

    #region Context Window Variables

    [Header("Ingame Windows")]
    public GameObject contextMenu;

    [Header("Debug")]
    public TMP_Text windowCoordinateText;

    public TMP_Text mouseCoordinateText;

    public TMP_InputField inputX;
    public TMP_InputField inputY;
    public TMP_InputField inputStep;

    private bool contextOpen;
    private bool isAnimating;

    #endregion Context Window Variables

    public void ContextWindowOpen(bool state)
    {
        if (!isAnimating)
        {
            contextOpen = state;
            isAnimating = true;

            if (contextOpen)
            {
                contextMenu.SetActive(true);
                LeanTween.scale(contextMenu, Vector3.one, .2f).setOnComplete(ContextWindowAnimDone).setOnCompleteParam(true);
            }
            else
            {
                LeanTween.scale(contextMenu, Vector3.zero, .2f).setOnComplete(ContextWindowAnimDone).setOnCompleteParam(false);
            }
        }
    }

    private void ContextWindowAnimDone(object isOpen)
    {
        if (!(bool)isOpen)
        {
            contextMenu.SetActive(false);
        }

        isAnimating = false;
    }

    public void CloseApp()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    #endregion Context Window

    #endregion Click Reponse

    #endregion Events

    #region Character Stuff

    #region Animation

    [Header("Animation")]
    public Animator animator;

    public SpriteRenderer sRenderer;
    public Transform charTransform;
    public Vector2 standingY;
    public Vector2 sittingY;

    #endregion Animation

    #region Character AI

    [Header("AI")]
    public float thinkingDelay = 10f;

    private bool AIisON = true;
    private AIState currentState;
    private bool canChangeState;

    private void TurnAIOn(bool state)
    {
        AIisON = state;
        if (AIisON)
        {
            ChangeState(AIState.Thinking);
        }
        else
        {
            ChangeState(AIState.Relaxing);
        }
    }

    private void ChangeState(AIState state)
    {
        Timing.KillCoroutines("ActionDelays");
        switch (state)
        {
            case AIState.Relaxing:
                Timing.RunCoroutine(RelaxingState(), "ActionDelays");
                break;

            case AIState.Thinking:
                ThinkingState();
                break;

            case AIState.Moving:
                MovingState();
                break;

            case AIState.Chasing:
                ChasingState();
                break;

            case AIState.Sitting:
                Timing.RunCoroutine(SittingState(), "ActionDelays");
                break;

            case AIState.Sleeping:
                SleepingState();
                break;

            case AIState.Hit:
                DijitakState();
                break;

            case AIState.Picked:
                Timing.RunCoroutine(DiangkatState());
                break;
        }
    }

    private IEnumerator<float> RelaxingState()
    {
        animator.SetInteger("AnimState", 0);
        charTransform.position = standingY;

        yield return Timing.WaitForSeconds(thinkingDelay);

        if (AIisON)
        {
            yield return Timing.WaitForSeconds(thinkingDelay);
            ChangeState(AIState.Thinking);
        }
        else
        {
            yield return Timing.WaitForOneFrame;
        }
    }

    private void ThinkingState()
    {
        if (!lagiDiangkat)
        {
            switch (UnityEngine.Random.Range(0, 2))
            {
                case 0:
                    ChangeState(AIState.Relaxing);
                    break;

                case 1:
                    ChangeState(AIState.Sitting);
                    break;
            }
        }
    }

    private void MovingState()
    {
        Timing.RunCoroutine(MoveByBits());
    }

    private void ChasingState()
    {
        Timing.RunCoroutine(ChaseMouse());
    }

    private IEnumerator<float> SittingState()
    {
        animator.SetInteger("AnimState", 2);
        charTransform.position = sittingY;

        if (AIisON)
        {
            yield return Timing.WaitForSeconds(thinkingDelay);
            ChangeState(AIState.Thinking);
        }
        else
        {
            yield return Timing.WaitForOneFrame;
        }
    }

    private void SleepingState()
    {

    }

    private void DijitakState()
    {
        PlayVoice(UnityEngine.Random.Range(0, 3));
        charTransform.position = standingY;
        animator.SetTrigger("Hit");
    }

    private IEnumerator<float> DiangkatState()
    {
        lagiDiangkat = true;
        bisaDijitak = false;
        charTransform.position = standingY;

        animator.SetInteger("AnimState", 3);
        AttachToMouse();

        while (lagiDiangkat)
        {
            print(lagiDiangkat);
            AttachToMouse();
            yield return Timing.WaitForOneFrame;
        }

        animator.SetInteger("AnimState", 0);

    }

    private void AttachToMouse()
    {
        Win32API.GetCursorPos(out mousePosX, out mousePosY);
        winCurrentPosX = mousePosX - winWidthOffset;
        winCurrentPosY = mousePosY - winHeightOffset;

        Win32API.MoveWindow(ownHwnd, winCurrentPosX, winCurrentPosY, winWidth, winHeight, false);
    }

    #endregion Character AI

    #region Voice n SFX

    [Header("Voice n SFX")]
    public AudioSource voiceSource;

    public AudioClip[] voiceClips;

    //0 1 - Uuh

    public void PlayVoice(int index)
    {
        if (voiceSource.isPlaying)
            voiceSource.Stop();
        voiceSource.clip = voiceClips[index];
        voiceSource.Play();
    }

    #endregion Voice n SFX

    #endregion Character Stuff

    #region Window Movement

    #region General Variable

    private bool isMoving = false;

    #endregion General Variable

    #region General Movement

    private void MoveWindowBy(int x, int y)
    {
        Rectangle tempRect;
        Win32API.GetWindowRect(ownHwnd, out tempRect);
        Win32API.MoveWindow(ownHwnd, tempRect.Left + x, tempRect.Top + y, winWidth, winHeight, false);
    }

    public void MoveToInstantly()
    {
        MoveWindowBy(int.Parse(inputX.text), int.Parse(inputY.text));
    }

    #endregion General Movement

    #region RandomMovement

    public void MoveToSlowly()
    {
        if (!isMoving)
        {
            ChangeState(AIState.Moving);
        }
    }

    public IEnumerator<float> MoveByBits()
    {
        isMoving = true;
        bool finished = false;
        Rectangle tempRect;
        Win32API.GetWindowRect(ownHwnd, out tempRect);

        int offsetX = int.Parse(inputX.text);
        int offsetY = int.Parse(inputY.text);
        int targetX = tempRect.Left + offsetX;
        int targetY = tempRect.Top + offsetY;
        int stepX = (offsetX > 0 ? 1 : -1) * int.Parse(inputStep.text);
        int stepY = (offsetY > 0 ? 1 : -1) * int.Parse(inputStep.text);
        bool stepXPositive = stepX > 0;
        bool stepYPositive = stepY > 0;

        sRenderer.flipX = stepXPositive;

        animator.SetInteger("AnimState", 1);

        while (!finished)
        {
            Win32API.GetWindowRect(ownHwnd, out tempRect);

            //calculate X Step
            if (stepX != 0)
            {
                if (targetX == tempRect.Left)
                {
                    stepX = 0;
                }
                else
                {
                    offsetX = tempRect.Left + stepX;

                    if (stepXPositive)
                    {
                        if (offsetX > targetX)
                            stepX = targetX - tempRect.Left;
                    }
                    else
                    {
                        if (offsetX < targetX)
                            stepX = targetX - tempRect.Left;
                    }
                }
            }

            //calculate Y Step
            if (stepY != 0)
            {
                if (targetY == tempRect.Top)
                {
                    stepY = 0;
                }
                else
                {
                    offsetY = tempRect.Top + stepX;

                    if (stepYPositive)
                    {
                        if (offsetY > targetY)
                            stepX = targetX - tempRect.Top;
                    }
                    else
                    {
                        if (offsetY < targetY)
                            stepY = targetY - tempRect.Top;
                    }
                }
            }

            //Move window
            if (stepX == 0 && stepY == 0)
            {
                finished = true;
            }
            else
            {
                Win32API.MoveWindow(ownHwnd, tempRect.Left + stepX, tempRect.Top + stepY, 400, 400, false);
            }

            yield return Timing.WaitForOneFrame;

        }

        ChangeState(AIState.Relaxing);
        isMoving = false;
    }

    #endregion RandomMovement

    #region Chasing Mouse Movement

    private bool isChasing = false;
    private bool chasingFinished = true;

    public void MoveToMouse()
    {
        if (!isChasing)
        {
            ContextWindowOpen(false);
            ChangeState(AIState.Chasing);
        }
    }

    public IEnumerator<float> ChaseMouse()
    {
        isChasing = true;
        Rectangle tempRect;
        Win32API.GetWindowRect(ownHwnd, out tempRect);
        Win32API.GetCursorPos(out mousePosX, out mousePosY);
        int targetX = mousePosX;
        int targetY = mousePosY;
        int offsetX = targetX - tempRect.Left;
        int offsetY = targetY - tempRect.Top;
        int stepX = (offsetX > 0 ? 1 : -1) * step;
        int stepY = (offsetY > 0 ? 1 : -1) * step;
        bool stepXPositive = stepX > 0;
        bool stepYPositive = stepY > 0;

        yield return Timing.WaitForSeconds(1.5f);

        chasingFinished = false;

        Timing.RunCoroutine(ChasingTimer(5f), "ChaseTimer");

        animator.SetInteger("AnimState", 1);

        while (!chasingFinished)
        {
            Win32API.GetWindowRect(ownHwnd, out tempRect);
            Win32API.GetCursorPos(out mousePosX, out mousePosY);

            targetX = mousePosX - winWidthOffset;
            targetY = mousePosY - winHeightOffset;
            offsetX = targetX - tempRect.Left;
            offsetY = targetY - tempRect.Top;
            stepX = (offsetX > 0 ? 1 : -1) * step;
            stepY = (offsetY > 0 ? 1 : -1) * step;
            stepXPositive = stepX > 0;
            stepYPositive = stepY > 0;

            //calculate X Step
            if (stepX != 0)
            {
                if (targetX == tempRect.Left)
                {
                    stepX = 0;
                }
                else
                {
                    offsetX = tempRect.Left + stepX;

                    sRenderer.flipX = !stepXPositive;

                    if (stepXPositive)
                    {
                        if (offsetX > targetX)
                            stepX = targetX - tempRect.Left;
                    }
                    else
                    {
                        if (offsetX < targetX)
                            stepX = targetX - tempRect.Left;
                    }
                }
            }

            //calculate Y Step
            if (stepY != 0)
            {
                if (targetY == tempRect.Top)
                {
                    stepY = 0;
                }
                else
                {
                    offsetY = (tempRect.Top) + stepY;

                    if (stepYPositive)
                    {
                        if (offsetY > targetY)
                            stepY = targetY - tempRect.Top;
                    }
                    else
                    {
                        if (offsetY < targetY)
                            stepY = targetY - tempRect.Top;
                    }
                }
            }

            //Move window
            if (stepX == 0 && stepY == 0)
            {

                chasingFinished = true;
            }
            else
            {
                Win32API.MoveWindow(ownHwnd, tempRect.Left + stepX, tempRect.Top + stepY, winWidth, winHeight, true);
                winCurrentPosX = tempRect.Left + stepX;
                winCurrentPosY = tempRect.Top + stepY;
            }

            yield return Timing.WaitForOneFrame;

        }

        Timing.KillCoroutines("ChaseTimer");

        ChangeState(AIState.Relaxing);

        isChasing = false;
    }

    private IEnumerator<float> ChasingTimer(float time)
    {
        yield return Timing.WaitForSeconds(time);
        chasingFinished = true;
    }

    #endregion Chasing Mouse Movement

    #endregion Window Movement
}