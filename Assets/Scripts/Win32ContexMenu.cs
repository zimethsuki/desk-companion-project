using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

public class Win32ContexMenu : IDisposable
{
    private const int WIDTH = 40;
    private const int HEIGHT = 40;
    private IntPtr hwnd;

    private bool AIisOn = true;

    private ContextMenuStrip contextMenu; // Context menu

    private ToolStripMenuItem menuItem_ChaseMouse; // Chase Mouse
    private ToolStripMenuItem menuItem_TurnAIOnOff; // Exit the program
    private ToolStripMenuItem menuItem_Quit; // Exit the program

    public void InitTray()
    {
        // int displayLength = Display.displays.Length;
        this.hwnd = Win32API.GetForegroundWindow();
        this.contextMenu = new ContextMenuStrip();
        this.menuItem_ChaseMouse = new ToolStripMenuItem();
        this.menuItem_TurnAIOnOff = new ToolStripMenuItem();
        this.menuItem_Quit = new ToolStripMenuItem();
        this.contextMenu.SuspendLayout();

        #region Menu List

        #region Init

        List<ToolStripMenuItem> menuItems = new List<ToolStripMenuItem>
            {
                this.menuItem_ChaseMouse,
                this.menuItem_TurnAIOnOff,
                this.menuItem_Quit
            };

        this.contextMenu.Items.AddRange(menuItems.ToArray());
        this.contextMenu.Size = new Size(181, (menuItems.Count * 22) + 20);

        #endregion Init

        #region Chase Mouse Command

        this.menuItem_ChaseMouse.Size = new Size(180, 22);
        this.menuItem_ChaseMouse.Text = "Chase Mouse";
        this.menuItem_ChaseMouse.Click += (sender, e) =>
        {
            this.PostNotification(ReMessageCenter.TRAY_WINDOW_EVENT, TrayMessages.ChaseMouse);
        };

        #endregion Chase Mouse Command

        #region Turn AI OnOff Command

        this.menuItem_TurnAIOnOff.Size = new Size(180, 22);
        this.menuItem_TurnAIOnOff.Text = AIisOn ? "Turn AI Off" : "Turn AI On";
        this.menuItem_TurnAIOnOff.Click += (sender, e) =>
        {
            AIisOn = !AIisOn;
            this.PostNotification(ReMessageCenter.TRAY_WINDOW_EVENT, TrayMessages.AIOnOff);
        };

        #endregion Turn AI OnOff Command

        #region Quit App

        this.menuItem_Quit.Size = new Size(180, 22);
        this.menuItem_Quit.Text = "Quit";
        this.menuItem_Quit.Click += (sender, e) =>
        {
            this.PostNotification(ReMessageCenter.TRAY_WINDOW_EVENT, TrayMessages.QuitApp);
        };

        #endregion Quit App

        #endregion Menu List

        this.contextMenu.ResumeLayout(false);
    }

    public void Show(int x, int y)
    {
        contextMenu.Show(x, y);
    }
    public void Hide()
    {
        contextMenu.Hide();
    }

    public void Dispose()
    {
        contextMenu?.Dispose();
        menuItem_ChaseMouse?.Dispose();
        menuItem_TurnAIOnOff?.Dispose();
        menuItem_Quit?.Dispose();
        this.hwnd = IntPtr.Zero;
    }
}