using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    public void DijitakDone()
    {
        this.PostNotification(ReMessageCenter.ANIMATION_EVENT_DONE, AnimationState.Dijitak);
    }
    public void NgejeblakDone()
    {
        this.PostNotification(ReMessageCenter.ANIMATION_EVENT_DONE, AnimationState.Jatoh);
    }
}
